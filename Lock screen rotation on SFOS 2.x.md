Lock screen rotation on SFOS 2.1.3.7, Sailfish OS

The option for landscape only view still rotates 180 degrees when I am on landscape only mode.
I have found that modifying the Page.qml file will patch the home screen rotation.
Rotation per application is not effected by this.

From Terminal:

    i) Backup Page.qml file
        $ devel-su
        # cd /usr/lib/qt5/qml/Sailfish/Silica/Page.qml
        # cp Page.qml Page.orig.qml
        # nano Page.qml

    ii) Modify Page.qml (line 115)
        Original line: return Orientation.All
        Updated line: return Orientation.LandscapeInverted
        
        Other views: Orientation.Portrait | Orientation.Landscape | Orientation.LandscapeInverted
    
    iii) Reboot phone to apply changes


Diff file:

             // No common supported orientations, let the page decide
             allowed = allowedOrientations
         }
-        return Orientation.All
+        return Orientation.LandscapeInverted
     }
     property alias _windowOrientation: orientationState.pageOrientation




**For SFOS 2.1.4.14 users**

Dietmar Schwertberger from OESF has informed me that the QML file is different. Here is the fix.

```
allowed = Orientation.LandscapeInverted | orientation === Orientation.Portrait | orientation === Orientation.PortraitInverted
```

to:

```
allowed = Orientation.LandscapeInverted
```
